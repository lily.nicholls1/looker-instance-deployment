USERNAME := lilynicholls

build-looker: ## Builds the k8s Looker container locally
	@docker build \
	--build-arg LOOKER_LICENSE=${LOOKER_LICENSE} \
	--build-arg LOOKER_EMAIL=${LOOKER_EMAIL} \
	--build-arg LOOKER_VERSION=${LOOKER_VERSION} \
	-t ${USERNAME}/looker .
