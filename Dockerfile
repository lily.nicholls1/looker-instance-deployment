FROM debian:buster

# install dependencies
RUN apt-get update && \
    apt-get upgrade && \
    apt-get install -y \
    wget software-properties-common gnupg2 git curl jq \
    chromium

# install OpenJDK
RUN wget -qO - https://adoptopenjdk.jfrog.io/adoptopenjdk/api/gpg/key/public | apt-key add - && \
    add-apt-repository --yes https://adoptopenjdk.jfrog.io/adoptopenjdk/deb/ && \
    apt-get update && apt-get install adoptopenjdk-8-hotspot -y

# add keepalive settings
ADD sysctl.conf /etc

RUN sed -i '$ d' /etc/security/limits.conf
RUN echo "looker           soft    nofile          4096" >> /etc/security/limits.conf
RUN echo "looker           hard    nofile          4096" >> /etc/security/limits.conf
RUN echo "# End of file" >> /etc/security/limits.conf

# setup looker user
RUN groupadd looker && \
    useradd -m -g looker looker && \
    usermod -aG sudo looker && \
    echo "looker:Docker!" | chpasswd

USER looker
WORKDIR /home/looker/looker

# download JAR files
# first authenticate with curl and then fetch URL from api_response to download the files
ARG LOOKER_LICENSE=""
ARG LOOKER_EMAIL=""
ARG LOOKER_VERSION="latest"


RUN curl -X POST -H 'Content-Type: application/json' -d """{\"lic\": \"${LOOKER_LICENSE}\", \"email\": \"${LOOKER_EMAIL}\", \"latest\":\"specific\", \"specific\":\"looker-${LOOKER_VERSION}.jar\" }""" https://apidownload.looker.com/download > api_response.json
RUN wget --quiet -O "looker.jar" "$(cat api_response.json | jq -r '.url')"
RUN curl -X POST -H 'Content-Type: application/json' -d """{\"lic\": \"${LOOKER_LICENSE}\", \"email\": \"${LOOKER_EMAIL}\", \"latest\":\"specific\", \"specific\":\"looker-dependencies-${LOOKER_VERSION}.jar\" }""" https://apidownload.looker.com/download > api_response.json
RUN wget --quiet -O "looker-dependencies.jar" "$(cat api_response.json | jq -r '.url')"

# download startup script
RUN curl -o /home/looker/looker/looker-startup https://raw.githubusercontent.com/looker/customer-scripts/master/startup_scripts/looker

# make the looker startup script executable
# change ownership username:group directory?
RUN chmod 0750 /home/looker/looker/looker-startup && \
    chown -R looker:looker /home/looker/looker

EXPOSE 9999
#
#CMD ["/home/looker/looker/looker-startup", "start"]