terraform {
  required_version = ">= 1.0.1"
  required_providers {
    google = {
      version = "4.12.0"
    }
    google-beta = {
      version = "4.12.0"
    }
  }
  backend "gcs" {
    // Must be created before running terraform init
    // contains default.tfstate
  bucket = "terraform-looker-instance-bucket"
 }
}

module "compute_engine" {
  source = "./modules/compute_engine"
  project = var.project
}