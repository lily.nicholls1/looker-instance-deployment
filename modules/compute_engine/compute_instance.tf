resource "google_compute_instance" "vm_instance" {
  project      = var.project
  name         = "looker"
  machine_type = "n1-standard-4"
  zone         = "europe-west2-c"

  labels = {"service": "looker"}

  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-10"
      size = 20
    }
  }

  network_interface {
    network = "default"
  }

//  metadata_startup_script = file("${path.module}/startup.sh")

  service_account {
    scopes = []
  }
}